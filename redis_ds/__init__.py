import redis

DEFAULT_CONNECTION_POOL = redis.ConnectionPool(host='localhost', port=6379)


class Serial(object):
    """ Serial counter using redis a lock """

    def __init__(self, name, namespace='', start=0, reset=False, redis_conn=None):
        self.key = 'serial:%s:%s' % (namespace, name)
        if redis_conn is None:
            redis_conn = redis.StrictRedis(connection_pool=DEFAULT_CONNECTION_POOL)
        self.redis_conn = redis_conn
        if reset:
            self.redis_conn.set(self.key, start)

    def get(self):
        return self.redis_conn.get(self.key)

    def next(self):
        return self.redis_conn.incr(self.key)


class PackedHash(object):
    """ Use a redis hash to pack multiple keys (improving space usage)"""

    def __init__(self, namespace='', partition_size=1000, redis_conn=None):
        self.namespace = namespace
        self.partition_size = partition_size
        if redis_conn is None:
            redis_conn = redis.StrictRedis(connection_pool=DEFAULT_CONNECTION_POOL)
        self.redis_conn = redis_conn

    def obj_to_int(self, obj):
        return obj.id

    def key_func(self, obj):
        q, r = divmod(self.obj_to_int(obj), self.partition_size)
        return ('%s:%d' % (self.namespace, q)), r

    def get(self, obj):
        key, hkey = self.key_func(obj)
        return self.redis_conn.hget(key, hkey)

    def set(self, obj, value):
        key, hkey = self.key_func(obj)
        return self.redis_conn.hset(key, hkey, value)

    def incr(self, obj, amount=1):
        key, hkey = self.key_func(obj)
        return self.redis_conn.hincrby(key, hkey, amount)

    def delete(self, obj):
        key, hkey = self.key_func(obj)
        return self.redis_conn.hdel(key, hkey)

    def __get__(self, obj, type=None):
        if obj is None:
            return self
        return self.get(obj)


class ExpiringSet(object):
    """ Packed version of using empty keys with a expire time """

    def __init__(self, namespace, redis_conn=None):
        self.sorted_set_name = namespace
        if redis_conn is None:
            redis_conn = redis.StrictRedis(connection_pool=DEFAULT_CONNECTION_POOL)
        self.redis_conn = redis_conn

    def _time(self):
        # Use redis server time instead of the one from server calling to avoid time
        return self.redis_conn.time()[0]

    def exists(self, key, single_use=True):
        score = self.redis_conn.zscore(self.sorted_set_name, key)
        exists = score is not None and score > self._time()
        if exists and single_use:
            self.remove(key)
        return exists

    def add(self, key, timeout_in_seconds=60):
        # Problem when if different servers have different times
        self.redis_conn.zadd(self.sorted_set_name, self._time() + timeout_in_seconds, key)
        self.purge()

    def remove(self, key):
        self.redis_conn.zrem(self.sorted_set_name, key)

    def purge(self):
        self.redis_conn.zremrangebyscore(self.sorted_set_name, 0, self._time())

    def top(self):
        self.purge()
        value = self.redis_conn.zrevrangebyscore(self.sorted_set_name, '+inf', '-inf', start=0, num=1)
        if not value:
            return None
        return value[0]
