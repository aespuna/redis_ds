from setuptools import find_packages, setup

setup(
    name='redis_ds',
    version='1.0.0',
    description='Some useful datastructures build in top of redis',
    packages=find_packages(),
    install_requires=['redis'],
)
