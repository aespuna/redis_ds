# Redis Datastructures

Some useful datastructures build on top of redis

## Packed hash (`PackedHash`)

A memory efficient hash (improvement on [this idea](http://instagram-engineering.tumblr.com/post/12202313862/storing-hundreds-of-millions-of-simple-key-value))

## Global counter (`Serial`)

Counter that uses redis as a lock

## Expiring Set (`ExpiringSet`)

A set that has a value specific TTL
